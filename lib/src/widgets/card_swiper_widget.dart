import 'package:flutter/material.dart';

import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:peliculas/src/models/pelicula_model.dart';
import 'package:carousel_slider/carousel_slider.dart';

class CardSwiper extends StatelessWidget {
  final List<Pelicula> peliculas;
  CarouselController buttonCarouselController = CarouselController();
  CardSwiper({@required this.peliculas});
  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;

    /*return Container(
      padding: EdgeInsets.only(top: 10.0),
      child: Swiper(
        itemWidth: _screenSize.width * 0.7,
        itemHeight: _screenSize.height * 0.5,

        layout: SwiperLayout.STACK,

        itemBuilder: (BuildContext context, int index) {
          return new Image.network(
            "http://via.placeholder.com/350x150",
            fit: BoxFit.fill,
          );
        },
        itemCount: 3,
        //pagination: new SwiperPagination(),
        //control: new SwiperControl(),
      ),
    );*/

    return Container(
        padding: EdgeInsets.only(top: 10.0),
        child: Column(
          children: <Widget>[
            Swiper(
              itemWidth: _screenSize.width * 0.7,
              itemHeight: _screenSize.height * 0.5,

              layout: SwiperLayout.STACK,

              itemBuilder: (BuildContext context, int index) {
                return ClipRRect(
                    borderRadius: BorderRadius.circular(20.0),
                    //child: Image.network(
                    //  "http://via.placeholder.com/350x150",
                    //  fit: BoxFit.cover,
                    //)
                    child: FadeInImage(
                      image: NetworkImage(peliculas[index].getPosterImg()),
                      placeholder: AssetImage('assets/no-image.jpg'),
                      fit: BoxFit.cover,
                    ));
              },
              itemCount: peliculas.length,
              //pagination: new SwiperPagination(),
              //control: new SwiperControl(),
            ),
            Text(
              "Slider de Peliculas",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
            ),
            /* CarouselSlider(
              options: CarouselOptions(height: 200.0),
              items: peliculas.map((i) {
                return Builder(
                  builder: (BuildContext context) {
                    return new Image.network(
                      peliculas[2].getPosterImg(),
                      fit: BoxFit.contain,
                    );
                  },
                );
              }).toList(),
            ), */
            CarouselSlider.builder(
              itemCount: peliculas.length,
              itemBuilder: (BuildContext context, int itemIndex) => Container(
                child: Image.network(
                  peliculas[itemIndex].getPosterImg(),
                  fit: BoxFit.contain,
                ),
              ),
              options: CarouselOptions(
                autoPlay: false,
                enlargeCenterPage: true,
                viewportFraction: 0.9,
                aspectRatio: 2.0,
                initialPage: 2,
              ),
              carouselController: buttonCarouselController,
            )
            /* Swiper(
              itemBuilder: (BuildContext context, int index) {
                return new Image.network(
                  peliculas[index].getPosterImg(),
                  fit: BoxFit.contain,
                );
              },
              itemCount: 10,
              itemWidth: 300.0,
              itemHeight: 200.0,
              layout: SwiperLayout.TINDER,
            ) */
          ],
        )
        /* Swiper(
        itemWidth: _screenSize.width * 0.7,
        itemHeight: _screenSize.height * 0.5,

        layout: SwiperLayout.STACK,

        itemBuilder: (BuildContext context, int index) {
          return ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              //child: Image.network(
              //  "http://via.placeholder.com/350x150",
              //  fit: BoxFit.cover,
              //)  
              child: FadeInImage(
                image: NetworkImage(peliculas[index].getPosterImg()),
                placeholder: AssetImage('assets/no-image.jpg'),
                fit: BoxFit.cover,
              ));
        },
        itemCount: peliculas.length,
        //pagination: new SwiperPagination(),
        //control: new SwiperControl(),
      ), */
        );
  }
}
